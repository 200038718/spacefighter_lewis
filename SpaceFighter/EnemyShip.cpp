
#include "EnemyShip.h"


EnemyShip::EnemyShip()
{
	// Assign the maximum number of hits taken by the enemy ship
	SetMaxHitPoints(1);
	// Set the radius around the ship for hit detection
	SetCollisionRadius(20);
}


void EnemyShip::Update(const GameTime *pGameTime)
{
	// Wait delay must be greater than 0
	if (m_delaySeconds > 0)
	{
		// Calculate the time elapsed by subtracting the Elapsed Time from DelaySeconds
		m_delaySeconds -= pGameTime->GetTimeElapsed();

		// Time has elasped past the Delay parameter
		if (m_delaySeconds <= 0)
		{
			// Set the EnemyShip to activate through the inheritated class- GameObject
			GameObject::Activate();
		}
	}

	// If the EnemyShip is active
	if (IsActive())
	{
		// Add the calculated elapsed time to the Activation variable of the EnemyShip object
		m_activationSeconds += pGameTime->GetTimeElapsed();

		// If the the EnemyShip is activate for longer than 2 seconds, deactivate the object
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate();
	}

	// do any updates that a normal ship would do.
	// (fire weapons, collide with objects, etc.)
	Ship::Update(pGameTime);
}


void EnemyShip::Initialize(const Vector2 position, const double delaySeconds)
{
	SetPosition(position);
	m_delaySeconds = delaySeconds;

	Ship::Initialize();
}


void EnemyShip::Hit(const float damage)
{
	Ship::Hit(damage);
}