
#include "BioEnemyShip.h"


BioEnemyShip::BioEnemyShip()
{
	SetSpeed(150);
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}


void BioEnemyShip::Update(const GameTime *pGameTime)
{
	// If the BioEnemyShip is active
	if (IsActive())
	{
		// Calculate the x axis from the time elasped since the game started
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		// Calculate the vertical speed of the ship
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;
		// Set the x position of the ship according to the speed and game clock
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed());

		// If the ship is no longer on screen, deactivate the object
		if (!IsOnScreen()) Deactivate();
	}

	// do any updates that a normal ship would do.
	// (fire weapons, collide with objects, etc.)
	EnemyShip::Update(pGameTime);
}


void BioEnemyShip::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
	}
}
